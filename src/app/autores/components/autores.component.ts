import { AutorService } from './../service/autor.service';
import { Component, OnInit } from '@angular/core';
import { Autor } from '../model/autor.model';

@Component({
  selector: 'app-autores',
  templateUrl: './autores.component.html',
  styleUrls: ['./autores.component.css']
})
export class AutoresComponent implements OnInit {
  autores: Autor[];
  errorMessage: string;

  constructor(private autorService: AutorService) { }

  ngOnInit(): void {
    this.obtenerAutores();

  }

  private obtenerAutores() {
    this.autorService.obtenerListadoAutores().subscribe((dato) => {
      this.autores = dato;
    },
      (error) => {
        this.errorMessage = error; // Mostrar el mensaje de error en la interfaz de usuario
      }
    )
  }
}
