export class Autor{
    id_autor:number;
    nombre: string;
    correo: string;
    nacionalidad: string;
    url_imagen: string;
}