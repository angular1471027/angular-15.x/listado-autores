import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Autor } from '../model/autor.model';

@Injectable({
  providedIn: 'root'
})
export class AutorService {

  // URL para obtener el listado de autores desde el backend
  private baseUrl = "http://localhost:9090/api/autores"

  constructor(private httpClient: HttpClient) { }

  obtenerListadoAutores(): Observable<Autor[]> {
    return this.httpClient.get<Autor[]>(`${this.baseUrl}`).pipe(
      catchError((error) => {
        console.error('Error en la solicitud HTTP:', error);
        // Puedes proporcionar una respuesta adecuada al usuario aquí
        // Por ejemplo, mostrar un mensaje de error en la interfaz de usuario
        return throwError(() => error);
      })
    );
  }  
}
