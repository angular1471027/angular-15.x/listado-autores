import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // Otras rutas si las tienes
  { path: 'autores', loadChildren: () => import('./autores/autores.module').then(m => m.AutoresModule) },
  { path: '', redirectTo: '/autores', pathMatch: 'full' }, // Ruta inicial redirige a /autores
  { path: 'libros', loadChildren: () => import('./libros/libros.module').then(j => j.LibrosModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
